CREATE TABLE `testowa`.`Fruits` (
  `fruitID` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `price` VARCHAR(10) NOT NULL,
  `stock` int(11) NOT NULL,
  `updated` DATE NOT NULL,
  PRIMARY KEY (`fruitID`));
