package pl.commons;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import pl.shop.model.Fruits;

public class JSONConverter {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	private static final Logger log = Logger.getLogger(JSONConverter.class);

	public static final List<Fruits> convertToList(final String params) {
		final List<Fruits> fruits = new ArrayList<Fruits>();
		try {
			JSONArray jsonArray = new JSONArray(params);
			extractItems(fruits, jsonArray);
		}
		catch (JSONException | ParseException e) {
			log.error("Unable to parse JSON ", e);
		}
		return fruits;
	}

	private static void extractItems(final List<Fruits> fruits, JSONArray jsonArray) throws ParseException {
		for (int i = 0; i < jsonArray.length(); i++) {
			final JSONObject item = jsonArray.getJSONObject(i);
			final String name = item.getString("name");
			final Double price = item.getDouble("price");
			final Integer stock = item.getInt("stock");
			final Date updated = sdf.parse(item.getString("updated"));

			Fruits f = new Fruits(name, price.toString(), stock, updated);
			fruits.add(f);
		}
	}
}
