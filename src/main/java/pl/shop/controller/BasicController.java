package pl.shop.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import pl.commons.JSONConverter;
import pl.shop.model.Fruits;
import pl.shop.services.FruitsService;

@Controller
public class BasicController {

	private static final Logger log = Logger.getLogger(BasicController.class);

	public BasicController() {
	}

	@Autowired
	FruitsService service;

	public BasicController(FruitsService service) {
		this.service = service;
	}

	@RequestMapping(value = { "/", "index" }, method = GET)
	public String homePage() {
		return "index";
	}

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView load() {
		return new ModelAndView("/table");
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public @ResponseBody String handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
		final String fileName = file.getOriginalFilename();

		final boolean validExtension = fileName.endsWith("json");
		if (!validExtension) {
			return "Please upload file with valid extension";
		}
		final byte[] bytes = file.getBytes();
		final String content = new String(bytes);
		
		final List<Fruits> entityList = JSONConverter.convertToList(content);
		service.saveFruitsItems(entityList);
		return "ITEMS UPDATED SUCCESSFULLY";
	}

	@RequestMapping(value = "/load/{firstName}", method = RequestMethod.GET)
	public ModelAndView loadByName(@PathVariable("firstName") String firstName, ModelMap model) {
		model.put("fruitList", service.findFruitByName(firstName));
		return new ModelAndView("/search", model);
	}

	@RequestMapping(value = "adminEdit", method = GET)
	public String adminItemsEdit(ModelMap model) {
		log.debug("Entered to admin page");
		List<Fruits> allFruits = service.showAllFruitItems();
		model.put("fruitList", allFruits);
		return "admin/editAdmin";
	}

	@RequestMapping(value = "/update/{id}", method = GET)
	public ModelAndView editFruitItem(@PathVariable("id") int fruitId) {
		log.debug("update fruit logic");
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("fruit", service.getFruitByID(fruitId));
		return new ModelAndView("admin/editFruit", model);
	}

	@RequestMapping(value = "/update/saveFruit", method = POST)
	public ModelAndView saveFruit(@ModelAttribute("fruit") @Valid Fruits fruit, BindingResult result) {

		if (result.hasErrors()) {
			log.debug("There are some errors");
			return new ModelAndView("admin/editFruit");
		}
		service.saveFruit(fruit);
		log.debug("save fruit logic");
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("fruitList", service.showAllFruitItems());
		model.put("msg", "Fruit " + fruit.getName() + " has been updated!!");
		ModelAndView mav = new ModelAndView("admin/editAdmin", model);

		return mav;
	}

	@RequestMapping(value = "/listAll", method = GET)
	public String listAll(ModelMap model) {
		List<Fruits> allFruits = service.showAllFruitItems();
		model.put("fruitList", allFruits);
		return "list";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
}
