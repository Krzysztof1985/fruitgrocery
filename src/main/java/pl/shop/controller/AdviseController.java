package pl.shop.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AdviseController {

	@ExceptionHandler(Exception.class)
	public String displayErrorPage() {
		return "error";
	}
}
