package pl.shop.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Fruits")
public class Fruits implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "fruitID")
	private int fruitID;

	@Column(name = "name")
	@NotNull(message = "Name cannot be empty!")
	private String name;

	@Column(name = "price")
	@NotNull(message = "Price cannot be empty!")
	@Pattern(regexp = "(\\d+)\\.*(\\d+)*", message = "Price must be valid for example 2.99")
	private String price;

	@Column(name = "stock")
	@NotNull(message = "Stock value cannot be empty!")
	private int stock;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "updated")
	@NotNull(message = "Date cannot be null or empty")
	private Date updated;

	public Fruits() {
	}

	public Fruits(String name, String price, int stock, Date updated) {
		this.name = name;
		this.price = price;
		this.stock = stock;
		this.updated = updated;
	}

	public int getFruitID() {
		return fruitID;
	}

	public void setFruitID(int fruitID) {
		this.fruitID = fruitID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	@Override
	public String toString() {
		return "Fruits [fruitID=" + fruitID + ", name=" + name + ", price=" + price + ", stock=" + stock + ", updated="
				+ updated + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + stock;
		result = prime * result + ((updated == null) ? 0 : updated.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (obj == null) return false;
		if (getClass() != obj.getClass()) return false;
		Fruits other = (Fruits) obj;
		if (name == null) {
			if (other.name != null) return false;
		}
		else if (!name.equals(other.name)) return false;
		if (price == null) {
			if (other.price != null) return false;
		}
		else if (!price.equals(other.price)) return false;
		if (stock != other.stock) return false;
		if (updated == null) {
			if (other.updated != null) return false;
		}
		else if (!updated.equals(other.updated)) return false;
		return true;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
}
