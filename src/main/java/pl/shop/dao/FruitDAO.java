package pl.shop.dao;

import java.util.List;

import pl.shop.model.Fruits;

public interface FruitDAO {

	public List<Fruits> getAllFruits();

	public Fruits findFruitById(final int fruitID);
	
	public void saveFruit(Fruits f);
	
	public List<Fruits> findFruitByName(final String name);
	
	public void saveFruitsItems(List<Fruits> fruits);
}
