package pl.shop.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pl.shop.model.Fruits;

@Repository
public class FruitDaoImpl implements FruitDAO {

	private static final Logger log = Logger.getLogger(FruitDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@Override
	public List<Fruits> getAllFruits() {
		final List<Fruits> list = getSession().createQuery("from Fruits f order by f.updated desc").list();
		return list;
	}

	@Override
	public Fruits findFruitById(int fruitID) {
		final Fruits fruit = (Fruits) getSession().get(Fruits.class, fruitID);
		return fruit;
	}

	@Override
	public void saveFruit(Fruits f) {
		getSession().saveOrUpdate(f);
	}

	@Override
	public List<Fruits> findFruitByName(String name) {
		return getSession().createCriteria(Fruits.class).add(Restrictions.like("name", name + "%").ignoreCase()).list();
	}

	@Override
	public void saveFruitsItems(List<Fruits> fruits) {
		log.debug("Save new fruits items, items to save " + fruits.size());
		getSession().createQuery("delete from Fruits").executeUpdate();
		fruits.stream().forEach(e -> saveFruit(e));
	}
}
