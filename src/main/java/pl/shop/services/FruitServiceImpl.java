package pl.shop.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pl.shop.dao.FruitDAO;
import pl.shop.model.Fruits;

@Service
@EnableTransactionManagement
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, isolation = Isolation.REPEATABLE_READ)
public class FruitServiceImpl implements FruitsService {

	private static final Logger log = Logger.getLogger(FruitServiceImpl.class);
	@Autowired
	FruitDAO fruitDao;

	@Override
	public List<Fruits> showAllFruitItems() {
		log.debug("Entered to display all fruits");
		return fruitDao.getAllFruits();
	}

	@Override
	public Fruits getFruitByID(int fruitID) {
		log.debug("Entered to display fruit by ID " + fruitID);
		return fruitDao.findFruitById(fruitID);
	}

	@Override
	@Transactional(readOnly = false, propagation = Propagation.NESTED)
	public void saveFruit(Fruits f) {
		log.debug("Entered to save fruit " + f);
		fruitDao.saveFruit(f);
	}

	@Override
	public List<Fruits> findFruitByName(String name) {
		log.debug("Entered to find fruit by name " + name);
		return fruitDao.findFruitByName(name);
	}

	@Override
	@Transactional(propagation = Propagation.NESTED, readOnly = false)
	public void saveFruitsItems(List<Fruits> fruits) {
		log.debug("Entered to save fruit list " + fruits.size());
		fruitDao.saveFruitsItems(fruits);
	}
}
