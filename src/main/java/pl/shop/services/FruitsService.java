package pl.shop.services;

import java.util.List;

import pl.shop.model.Fruits;

public interface FruitsService {

	public List<Fruits> showAllFruitItems();

	public Fruits getFruitByID(final int fruitID);

	public void saveFruit(Fruits f);

	public List<Fruits> findFruitByName(final String name);

	public void saveFruitsItems(List<Fruits> fruits);
}
