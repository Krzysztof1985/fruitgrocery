<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!DOCTYPE HTML>
<html>
<head>
<title>Search For Product</title>

<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
<script src="<c:url value="/resources/js/jquery.js"/>"></script>
<script src="<c:url value="/resources/js/contact.js"/>"></script>
<c:set var="req" value="${pageContext.request}" />
</head>
<body>
<p id="contextPath" style="display: none;">${pageContext.request.contextPath}</p>
	<div id="container">
		<h2>Find Fruit By Name</h2>

		<label for="search">Search</label> <input type="text" id="search"
			name="search">
		<div id="info"></div>
		<table id="loadTable" class="table tr">
			<thead>
				<tr>
					<th>ID</th>
					<th>Fruit Name</th>
					<th>Fruit price</th>
					<th>Fruit stock</th>
					<th>Last updated</th>
				</tr>
			</thead>
			<tbody id="tbody">
				<jsp:include page="search.jsp" />
			</tbody>
		</table>
	</div>
</body>
</html>