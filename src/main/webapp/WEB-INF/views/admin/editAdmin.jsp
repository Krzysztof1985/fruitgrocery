<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Admin Edit</title>
</head>
<div style="color: red;">${msg}</div>
<body>
	<table border="1px solid black" class="table tr">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Price</th>
				<th>Stock</th>
				<th>Last updated</th>
				<th>Edit item</th>
			</tr>
		</thead>
		<c:forEach items="${fruitList}" var="fruit">
			<tr>
				<td><c:out value="${fruit.fruitID}" /></td>
				<td><c:out value="${fruit.name}" /></td>
				<td><c:out value="${fruit.price}" /></td>
				<td><c:out value="${fruit.stock}" /></td>
				<td><fmt:formatDate pattern="dd/MM/yyyy"
						value="${fruit.updated}" /></td>
				<td><a
					href="${pageContext.request.contextPath}/update/${fruit.fruitID}">Edit</a></td>
			</tr>
		</c:forEach>
	</table>

	<br />
	<a href="${pageContext.request.contextPath}/index.html">Back to
		homepage</a>
</body>
</html>