<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
.error {
	color: #ff0000;
}

.errorblock {
	color: #000;
	background-color: #ffEEEE;
	border: 3px solid #ff0000;
	padding: 8px;
	margin: 16px;
	width: 120px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Edit Form</title>
</head>
<body>
	<form:form action="saveFruit.html" method="POST" modelAttribute="fruit">
		<form:errors path="*" cssClass="errorblock" element="div" />
		<table>
			<tr>
				<td>Fruit Id</td>
				<td><form:input path="fruitID" readonly="true" />
			</tr>
			<tr>
				<td>Fruit Name</td>
				<td><form:input path="name" readonly="true" />
			</tr>
			<tr>
				<td>Fruit price</td>
				<td><form:input path="price" />
			</tr>
			<tr>
				<td>Stock</td>
				<td><form:input path="stock" />
			</tr>
			<tr>
				<td>updated</td>
				<td><form:input path="updated" readonly="true" />
			</tr>
		</table>
		<input type="submit" value="SAVE" />
	</form:form>
</body>
</html>