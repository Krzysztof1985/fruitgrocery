<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<c:forEach items="${fruitList}" var="fruit">
			<tr>
				<td><c:out value="${fruit.fruitID}" /></td>
				<td><c:out value="${fruit.name}" /></td>
				<td><c:out value="${fruit.price}" /></td>
				<td><c:out value="${fruit.stock}" /></td>
				<td><fmt:formatDate pattern="dd/MM/yyyy" value="${fruit.updated}" /></td>
			</tr>
		</c:forEach>