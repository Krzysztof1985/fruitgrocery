<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="<c:url value="/resources/css/style.css"/>" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Simple Fruit Grocery</title>
</head>
<body>
	<p>
		<a href="listAll.html">Display all fruits</a>
	</p>
	<p>
		<a href="adminEdit.html">Display to edit</a>
	</p>
	<p>
		<a href="search.html">Search product by name</a>
	<form method="POST" enctype="multipart/form-data"
		action="./upload.html">
		File to upload: <input type="file" name="file"><br /> <input
			type="submit" value="Upload"> Press here to upload the file!
	</form>
</body>
</html>