function clearInputs() {
	$('input[id*="Input"]').each(function() {
		$(this).val('');
	});
}

$(document).ready(function() {
	$('#search').keyup(function(e) {
		var filter = $('#search').val();
		if (filter.length > 0) {
			loadTable(filter);
		} else {
			$('tr[id*="tr_"]').remove();
		}
	});
});

function loadTable(filter) {
	// var url = window.location.pathname + "/contact/load/" + filter;
	var path = $("#contextPath").text();
	var url = path+"/load/" + filter;
	$('#tbody').load(url, function(response, status, xhr) {
		if (status == "error") {
			var msg = "Sorry but there was an error: ";
			$("#info").html(msg + xhr.status + " " + xhr.statusText);
		}
	});

	return false;
}
