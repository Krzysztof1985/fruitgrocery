CREATE TABLE `testowa`.`Fruits` (
  `fruitID` INT NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `price` DECIMAL(4,2) NOT NULL,
  `stock` INT NOT NULL,
  `updated` DATE NOT NULL,
  PRIMARY KEY (`fruitID`));
