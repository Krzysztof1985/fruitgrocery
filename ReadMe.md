Appliation Grocery:

Search fruit by name
Display all Fruits ordered by last updated
Edit admin option (without login/authentication)
Upload JSON file with fruits and clear db, insert to db data from JSON.

How to run:

go to command line and execute command:
mvn clean install | mvn jetty:run

Or run locally: upload war file form target to server and enter the url

localhost:8080/fruitGrocery/



Some assumptions:

Column price - declared as a String, because Validation Regexp works only for String/Varchar columns.
Editing this data will be through web app so validation will be works properly, but manual manipulation on db is possible.

I assumed, that upload/insert to db from JSON it will be allways works properly, in real world I will not declared methods as a void, but with some boolean (success-true, failed - false)

I don't have enough time to do some more logging
